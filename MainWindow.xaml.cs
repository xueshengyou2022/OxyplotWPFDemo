﻿using OxyplotWPFDemo.Controls;
using System.Diagnostics;
using System.Windows;

namespace OxyplotWPFDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        散点图 sd = new 散点图();
        柱状图 zz = new 柱状图();

        public MainWindow()
        {
            InitializeComponent();
            this.main.Content = sd;
            //以e为底
            var a = Math.Log(100);
            //以20为底
            var aa = Math.Log(400,20);
            var b = Math.Log10(100);
            //var c = Math.Log2(8);
            //-----4.605170185988092----->2---2---3
            //Debug.WriteLine($"-----{a}----->{aa}---{b}---{c}");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.main.Content = sd;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(1);
            this.main.Content = zz;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(2);
            this.main.Content = zz;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(3);
            this.main.Content = zz;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(4);
            this.main.Content = zz;
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(5);
            this.main.Content = zz;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(6);
            this.main.Content = zz;
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(7);
            this.main.Content = zz;
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(8);
            this.main.Content = zz;
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            zz.ChangePlot(9);
            this.main.Content = zz;
        }
        //三次方贝塞尔曲线
        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            Window1 window1 = new Window1();
            window1.ShowDialog();
        }
        //ScottPlot库使用
        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            ScottPlotWindow window1 = new ScottPlotWindow();
            window1.ShowDialog();
        }
    }
}