﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OxyplotWPFDemo
{
    /// <summary>
    /// Window1.xaml 的交互逻辑
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();

            Loaded += Window1_Loaded;
        }

        private void Window1_Loaded(object sender, RoutedEventArgs e)
        {
            PathFigure pf = new PathFigure();
            pf.StartPoint = new Point(0, 0);
            BezierSegment bzer = new BezierSegment(new Point(250, 0), new Point(50, 200), new Point(300, 200), true);
            pf.Segments.Add(bzer);
            List<PathFigure> lstFigure = new List<PathFigure>();
            lstFigure.Add(pf);
            PathGeometry pg = new PathGeometry(lstFigure.ToArray());
            Path pa = new Path { Stroke = Brushes.Red, StrokeThickness = 2 };
            pa.Data = pg;
            // 显示
            this.grid.Children.Add(pa);
        }
    }
}
