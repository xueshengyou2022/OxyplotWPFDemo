﻿using ScottPlot.Plottables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OxyplotWPFDemo
{
    /// <summary>
    /// ScottPlotWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ScottPlotWindow : Window
    {
        public ScottPlotWindow()
        {
            InitializeComponent();

            #region 添加点

            var myScatter = myPlot.Plot.Add.Scatter(1, 1);
            myScatter.LineStyle.Width = 5;
            myScatter.LineStyle.Color = ScottPlot.Colors.GreenYellow;
            myScatter.MarkerStyle.Fill.Color = ScottPlot.Colors.Red;
            myScatter.MarkerStyle.Size = 10;

            myScatter = myPlot.Plot.Add.Scatter(2, 1.2);
            myScatter.LineStyle.Width = 5;
            myScatter.LineStyle.Color = ScottPlot.Colors.GreenYellow;
            myScatter.MarkerStyle.Fill.Color = ScottPlot.Colors.Red;
            myScatter.MarkerStyle.Size = 10;

            myScatter = myPlot.Plot.Add.Scatter(3, 3);
            myScatter.LineStyle.Width = 5;
            myScatter.LineStyle.Color = ScottPlot.Colors.GreenYellow;
            myScatter.MarkerStyle.Fill.Color = ScottPlot.Colors.Red;
            myScatter.MarkerStyle.Size = 10;

            myScatter = myPlot.Plot.Add.Scatter(4, 7);
            myScatter.LineStyle.Width = 5;
            myScatter.LineStyle.Color = ScottPlot.Colors.GreenYellow;
            myScatter.MarkerStyle.Fill.Color = ScottPlot.Colors.Red;
            myScatter.MarkerStyle.Size = 10;

            myScatter = myPlot.Plot.Add.Scatter(5, 7);
            myScatter.LineStyle.Width = 5;
            myScatter.LineStyle.Color = ScottPlot.Colors.GreenYellow;
            myScatter.MarkerStyle.Fill.Color = ScottPlot.Colors.Red;
            myScatter.MarkerStyle.Size = 10;

            #endregion

            #region 测试1

            ////测试1
            //double[] dataX = { 1, 2, 3, 4, 5 };
            //double[] dataY = { 1, 4, 9, 16, 25 };
            //myPlot.Plot.Add.Scatter(dataX, dataY);
            //myPlot.Refresh();
            #endregion

            #region 测试2 

            ////测试2 
            //// Functions are defined as delegates with an input and output
            //var func1 = new Func<double, double>((x) => Math.Sin(x) * Math.Sin(x / 2));
            //var func2 = new Func<double, double>((x) => Math.Sin(x) * Math.Sin(x / 3));
            //var func3 = new Func<double, double>((x) => Math.Cos(x) * Math.Sin(x / 5));

            //// Add functions to the plot
            //myPlot.Plot.Add.Function(func1);
            //myPlot.Plot.Add.Function(func2);
            //myPlot.Plot.Add.Function(func3);

            //// Manually set axis limits because functions do not have discrete data points
            //myPlot.Plot.Axes.SetLimits(-10, 10, -1.5, 1.5);
            #endregion

            #region 测试3
            double a = 1.099995;
            double b = 31.0305;
            double c = 3.072862;
            double d = 7.000825;
            Func<double, double> modelFunc = (x) => (a - d) / (1 + Math.Pow((x / c), b)) +d;
            myPlot.Plot.Add.Function(modelFunc);
            myPlot.Plot.Axes.SetLimits(0, 7.5,0.8, 7.5);
            #endregion
        }
    }
}
